<?php

declare(strict_types=1);

namespace Drupal\ckeditor\Config;

/**
 * Defines the settings config interface.
 */
interface SettingsConfigHandlerInterface {

  /**
   * Getter for the license key.
   *
   * @return string|null
   *   The license key if defined, null otherwise.
   */
  public function getLicenseKey(): ?string;

}
