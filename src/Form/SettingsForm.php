<?php

declare(strict_types=1);

namespace Drupal\ckeditor\Form;

use Drupal\Core\Cache\Cache;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

class SettingsForm extends ConfigFormBase {

    /**
     * Minimum required length of the License key.
     */
    const LICENSE_KEY_MIN_LENGTH = 48;

    /**
     * @inheritDoc
     */
    protected function getEditableConfigNames() {
      return [
        $this->getFormId()
      ];
    }

    /**
     * @inheritDoc
     */
    public function getFormId() {
      return 'ckeditor.lts.settings';
    }

    /**
    * @inheritDoc
    */
    public function buildForm(array $form, FormStateInterface $form_state) {
      $config = $this->config(($this->getFormId()));
      $form['license_key'] = [
        '#type' => 'textfield',
        '#maxlength' => 512,
        '#required' => FALSE,
        '#title' => $this->t('License key'),
        '#description' => 'LTS license key',
        '#default_value' => $config->get('license_key')
      ];

      $form['advanced'] = [
        '#type' => 'details',
        '#title' => $this->t('Advanced settings'),
        '#open' => FALSE,
       ];

      $form['advanced']['clear_cache'] = [
        '#type' => 'submit',
        '#suffix' => $this->t('<div class="form-item__description">The user cache will be cleared. It is necessary to clear the cache if you already have the old ckeditor module installed.</div>'),
        '#value' => $this->t('Clear cache'),
        '#submit' => ['::clearUserCache'],
      ];

      return parent::buildForm($form, $form_state);
    }

  /**
   * @param array $form
   * @param FormStateInterface $form_state
   * @return void
   */
    public function validateForm(array &$form, FormStateInterface $form_state) {
      $this->processCleanValues($form_state);
      $license_key = $form_state->getValue('license_key');

      if (!empty($license_key) && strlen($license_key) < self::LICENSE_KEY_MIN_LENGTH) {
        $form_state->setErrorByName('license_key', $this->t('@name length is invalid (minimum @num characters required)', [
          '@name' => 'License key',
          '@num' => self::LICENSE_KEY_MIN_LENGTH,
        ]));
      }
      parent::validateForm($form, $form_state);
    }

  /**
    * @inheritDoc
    */
    public function submitForm(array &$form, FormStateInterface $form_state) {
      $cleanValues = $this->processCleanValues($form_state);

      $this->config($this->getFormId())
        ->setData($cleanValues)
        ->save();

      $invalidate_tags = [
        'ckeditor_plugins',
        'editor_plugins',
        'filter_plugins',
      ];

      Cache::invalidateTags($invalidate_tags);

      parent::submitForm($form, $form_state);
    }

  /**
   * Additionally cleans up the form state values.
   *
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   Form state object that values should be cleaned up additionally.
   *
   * @return array
   *   Form state clean values.
   */
  protected function processCleanValues(FormStateInterface $form_state): array {
    $clean_values = $form_state->cleanValues()->getValues();

    foreach ($clean_values as &$value) {
      if (is_string($value)) {
        $value = trim($value);
      }
    }
    $form_state->setValues($clean_values);

    return $clean_values;
  }

  /**
   * Clear user cache for all major platforms.
   *
   * @param array $form
   * @param FormStateInterface $form_state
   * @return void
   */
  public function clearUserCache(array &$form, FormStateInterface $form_state):void {
    $userCaches = [
      'apcu_clear_cache',
      'wincache_ucache_clear',
    ];
    array_map('call_user_func', array_filter($userCaches, 'is_callable'));

    $this->messenger()->addStatus($this->t('Cache cleared.'));
  }
}
