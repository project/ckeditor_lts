# CKEditor 4 LTS (“Long Term Support”)

CKEditor 4 was a rich-content WYSIWYG editor launched in 2012 and reached its End of Life (EOL) on June 30, 2023. For Drupal exclusively, security updates are extended until the end of 2023 for free.

This special edition, CKEditor 4 LTS ("Long Term Support"), is available under commercial terms ("Extended Support Model") for anyone looking to extend the coverage of security updates and critical bug fixes.

With CKEditor 4 LTS, security updates, and critical bug fixes are guaranteed until December 2026.

CKEditor LTS requires a license key. If you’re interested in the LTS service for CKEditor 4, please contact our sales team: [https://ckeditor.com/contact-sales/#send-message](https://ckeditor.com/contact-sales/#send-message)

## Purpose of this module

Starting from version 4.23.0-LTS, CKEditor 4 is published under commercial terms of the Extended Support Model contract. All customers who obtained a special license extending the editor support after its End Of Life are obligated to provide a license key.

If you already acquired the Extended Support Model, you can integrate CKEditor 4 LTS (”Long Term Support”) today, by installing this module:

 * the editor will be protected against security vulnerabilities that were reported after June 30, 2023;
 * you will be able to enter a license key required to initialize the editor;
 * if you already use CKEditor 4 in the Open-Source version, this module will override the existing one with the LTS build of CKEditor 4;


Please note, that for Drupal exclusively, security updates are extended until the end of 2023 for free. However, if you are already a member of the Extended Support Model with a valid license beyond 2023, we recommend taking steps to secure your editor instance today.

## Installation:
If you have not used the ckeditor module before, simply download the `ckeditor_lts` module and enable it, then go to `/admin/config/ckeditor-lts/settings`, and enter your license key.
If you use Drupal 9 or have already installed https://www.drupal.org/project/ckeditor you can enable this module in two ways.

### First method
* Download `ckeditor_lts` into your project and make sure to clear an APCu cache or any alternative you use.
* In the case of APCu, you can visit `/admin/config/ckeditor-lts/settings` and click the clear cache button under advanced settings.
* Go to `/admin/config/ckeditor-lts/settings` and enter your license key

### Second method
* Export all configs related to editor and text formats.
* Uninstall CKEditor module and make sure that ckeditor directory is deleted from the project files.
* Add `ckeditor_lts` module to your project and enable it.
* Import all configs related to editor and text formats.
* Go to `/admin/config/ckeditor-lts/settings` and enter your license key.

### Uninstallation
In case you don’t want to go back to the non-LTS version of CKEditor 4 and completely remove the module (for example in case of migration to CKEditor 5) uninstall the module as any other module. Otherwise please follow instructions below:
* Make sure that the old CKEeditor module is present in contrib modules directory.
* Remove the `ckeditor_lts` module from files or run composer remove `drupal/ckeditor_lts`
* Clear APCu (or any alternative you use) cache manually or using Drupal rebuild (instruction below)


## Clearing APCu/WinCache using Drupal rebuild
APCu can be cleared with Drupal. In order to achieve this, access to the server's console is required.
* Access the site’s server console
* Run a shell script located in `{DRUPAL_ROOT}/web/core/scripts/rebuild_token_calculator.sh`. The output will contain the timestamp and token query parameters. Copy the whole string.
* In a browser window go to `/rebuild.php?{COPIED_VALUE}`
* The user cache will be cleared. It is necessary to clear the cache if you already had the old CKEditor module installed.


## Drupal 7
Drupal 7 allows you to provide the CKEditor 4 library URL and enter a license key with existing forms. You can find the guide [here](https://www.drupal.org/docs/extending-drupal/contributed-modules/contributed-module-documentation/ckeditor-lts/drupal-7-configuration).
